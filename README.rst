encounterplanner
################

Python program for planning D&D encounters, using the tables given in the
Dungeon Master Guide.
