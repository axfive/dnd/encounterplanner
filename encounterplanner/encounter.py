from collections import namedtuple
from collections.abc import MutableSequence
from weakref import ref
from .monster import CR_LIST

def lerp(low, high, i):
    '''Linear interpolation'''
    if high < low:
        raise ValueError(f'high ({high}) is not higher than low ({low})')

    return low + i * (high - low)

def rlerp(low, high, mid):
    '''Reverse linear interpolation'''
    if high < low:
        raise ValueError(f'high ({high}) is not higher than low ({low})')

    return (mid - low) / (high - low)

def pairs(iterable):
    '''Iterate an iterable by each consecutive pair of elements'''

    i = iter(iterable)
    first = next(i, None)
    for second in i:
        yield first, second
        first = second

def exp_to_cr(exp):
    '''Get floating point CR from the input exp value.  Interpolate if none are matched exactly'''
    if CR_LIST[0][1] >= exp:
        return 0.0
    else:
        for low, high in pairs(CR_LIST):
            low_cr = float(low[0])
            low_exp = low[1]
            high_cr = float(high[0])
            high_exp = high[1]

            if low_exp <= exp <= high_exp or high[0] == 30:
                i = rlerp(mid=exp, high=high_exp, low=low_exp)
                return lerp(i=i, low=low_cr, high=high_cr)

class Threshold(namedtuple('Threshold', ['easy', 'medium', 'hard', 'deadly'])):
    __slots__ = ()

    def __add__(self, other):
        return Threshold(
            easy=self.easy + other.easy,
            medium=self.medium + other.medium,
            hard=self.hard + other.hard,
            deadly=self.deadly + other.deadly,
        )

    def which(self, value):
        '''Return the string for the threshold matched by the value'''

        if value <= 0:
            return 'peaceful'
        if value < self.easy:
            return 'trivial'
        elif value < self.medium:
            return 'easy'
        elif value < self.hard:
            return 'medium'
        elif value < self.deadly:
            return 'hard'
        else:
            return 'deadly'

THRESHOLDS = {
    1: Threshold(easy=25, medium=50, hard=75, deadly=100),
    2: Threshold(easy=50, medium=100, hard=150, deadly=200),
    3: Threshold(easy=75, medium=150, hard=225, deadly=400),
    4: Threshold(easy=125, medium=250, hard=375, deadly=500),
    5: Threshold(easy=250, medium=500, hard=750, deadly=1100),
    6: Threshold(easy=300, medium=600, hard=900, deadly=1400),
    7: Threshold(easy=350, medium=750, hard=1100, deadly=1700),
    8: Threshold(easy=450, medium=900, hard=1400, deadly=2100),
    9: Threshold(easy=550, medium=1100, hard=1600, deadly=2400),
    10: Threshold(easy=600, medium=1200, hard=1900, deadly=2800),
    11: Threshold(easy=800, medium=1600, hard=2400, deadly=3600),
    12: Threshold(easy=1000, medium=2000, hard=3000, deadly=4500),
    13: Threshold(easy=1100, medium=2200, hard=3400, deadly=5100),
    14: Threshold(easy=1250, medium=2500, hard=3800, deadly=5700),
    15: Threshold(easy=1400, medium=2800, hard=4300, deadly=6400),
    16: Threshold(easy=1600, medium=3200, hard=4800, deadly=7200),
    17: Threshold(easy=2000, medium=3900, hard=5900, deadly=8800),
    18: Threshold(easy=2100, medium=4200, hard=6300, deadly=9500),
    19: Threshold(easy=2400, medium=4900, hard=7300, deadly=10900),
    20: Threshold(easy=2800, medium=5700, hard=8500, deadly=12700),
}

MULTIPLIERS = [0.5, 1, 1.5, 2, 2.5, 3, 4, 5]
MULTIPLIER_SELECTOR = [0, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5]

def multiplier(playercount, monstercount):
    '''Find the multiplier based on the player count and monster count'''

    if monstercount > 15:
        monstercount = 15
    monstercount -= 1

    index = MULTIPLIER_SELECTOR[monstercount]

    if playercount < 3:
        index += 2
    elif playercount < 6:
        index += 1

    return MULTIPLIERS[index]

def party_threshold(levels):
    '''Calculate the total difficulty threshold based on player levels'''

    return sum(
        (THRESHOLDS[level] for level in levels),
        Threshold(easy=0, medium=0, hard=0, deadly=0),
    )

class Encounter(MutableSequence):
    def __init__(self, monsters=None, name='Encounter'):
        self.name = name
        self.__monsters = []
        if monsters:
            for monster in monsters:
                if isinstance(monster, EncounterMonster):
                    monster.encounter = self
                    self.__monsters.append(monster)
                else:
                    raise RuntimeError('Will not accept a monster that is not an EncounterMonster')

    def __getitem__(self, index):
        return self.__monsters.__getitem__(index)

    def __setitem__(self, index, monster):
        if isinstance(monster, EncounterMonster):
            monster.encounter = self
            self.__monsters[index] = monster
        else:
            # Assume slice assignment
            for each in monster:
                if isinstance(each, EncounterMonster):
                    each.encounter = self
                else:
                    raise RuntimeError('Will not accept a monster that is not an EncounterMonster')
            self.__monsters.__setitem__(index, monster)

    def __delitem__(self, index):
        self.__monsters.__delitem__(index)

    def __len__(self):
        return self.__monsters.__len__()

    def insert(self, index, monster):
        if isinstance(monster, EncounterMonster):
            monster.encounter = self
            self.__monsters.insert(index, monster)
        else:
            raise RuntimeError('Will not accept a monster that is not an EncounterMonster')

    @property
    def experience(self):
        return sum(monster.experience for monster in self.__monsters)

    @property
    def cr(self):
        # TODO: calculate a fun CR here
        return 'cr'

    def __str__(self):
        return f'<Encounter {self.name} {self.__monsters}>'

class EncounterMonster:
    def __init__(self, monster, encounter=None):
        self.__monster = ref(monster)
        if encounter is None:
            self.__encounter = None
        else:
            self.__encounter = ref(encounter)

    @property
    def encounter(self):
        if self.__encounter is not None:
            return self.__encounter()

    @encounter.setter
    def encounter(self, value):
        if value is None:
            self.__encounter = None
        else:
            self.__encounter = ref(value)

    @property
    def monster(self):
        return self.__monster()

    @property
    def name(self):
        return self.monster.name

    @property
    def experience(self):
        return self.monster.experience

    @property
    def cr(self):
        return self.monster.cr

    def __str__(self):
        return f'<EncounterMonster {self.name}>'
