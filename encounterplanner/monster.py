#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

try:
    from importlib import resources
except ImportError:
    import importlib_resources as resources

from collections.abc import Mapping
from pathlib import Path
from fractions import Fraction
import json
from appdirs import user_data_dir
from .file import safe_write

CR_MAP = {
    Fraction(0): 10,
    Fraction(1, 8): 25,
    Fraction(1, 4): 50,
    Fraction(1, 2): 100,
    Fraction(1): 200,
    Fraction(2): 450,
    Fraction(3): 700,
    Fraction(4): 1100,
    Fraction(5): 1800,
    Fraction(6): 2300,
    Fraction(7): 2900,
    Fraction(8): 3900,
    Fraction(9): 5000,
    Fraction(10): 5900,
    Fraction(11): 7200,
    Fraction(12): 8400,
    Fraction(13): 10000,
    Fraction(14): 11500,
    Fraction(15): 13000,
    Fraction(16): 15000,
    Fraction(17): 18000,
    Fraction(18): 20000,
    Fraction(19): 22000,
    Fraction(20): 25000,
    Fraction(21): 30000,
    Fraction(22): 41000,
    Fraction(23): 50000,
    Fraction(24): 62000,
    Fraction(25): 75000,
    Fraction(26): 90000,
    Fraction(27): 105000,
    Fraction(28): 120000,
    Fraction(29): 135000,
    Fraction(30): 155000,
}

CR = sorted(CR_MAP)

CR_LIST = tuple((cr, CR_MAP[cr]) for cr in CR)

class Monster:
    def __init__(self, name, cr):
        self.name = name
        self.cr = cr
        self.__experience = CR_MAP[self.cr]

    @property
    def cr(self):
        return self.__cr

    @cr.setter
    def cr(self, value):
        value = Fraction(value)
        if value not in CR_MAP:
            raise RuntimeError(f'CR {value} not a legal Challenge Rating')
        self.__cr = value
        self.__experience = CR_MAP[value]

    @property
    def experience(self):
        return self.__experience

    def __str__(self):
        return f'<Monster {self.name}>'

_loaded_monsters = None

_monsters_path = Path(user_data_dir('encounterplanner', 'Axfive')) / 'monsters.json'

def load_monsters():
    '''Load the monsters from the user config path.

    If the file does not exist, prime it with SRD data.
    '''
    global _loaded_monsters
    if _loaded_monsters is None:
        if not _monsters_path.exists():
            _monsters_path.parent.mkdir(parents=True, exist_ok=True)
            with resources.open_text('encounterplanner', 'srd_monsters.json') as srd, safe_write(_monsters_path) as file:
                file.write(srd.read())

        with _monsters_path.open() as file:
            _loaded_monsters = {key: Fraction(value) for key, value in json.load(file).items()}
    return _loaded_monsters

def update_monsters(monsters):
    '''Update both the loaded monsters list and the file with a dict of user-given monsters.
    The passed in value may be a dict, which will be used directly, or a Path
    or string object, which will be used as a file path to open a json file to
    load in as such a dict.
    '''

    if not isinstance(monsters, Mapping):
        with open(monsters) as file:
            monsters = json.load(file)

    # In case the monsters haven't been loaded/initialized yet
    previous_monsters = load_monsters()
    previous_monsters.update(monsters)

    with safe_write(_monsters_path) as file:
        json.dump({key: str(value) for key, value in previous_monsters.items()}, file, indent=2)
