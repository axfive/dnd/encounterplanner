#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

import wx
from gettext import gettext as _
from encounterplanner.monster import CR, load_monsters
from fractions import Fraction

class AddPlayerDialog(wx.Dialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, title=_('Add New Player'), **kwargs)

        top_sizer = wx.BoxSizer(wx.VERTICAL)

        top_sizer.AddStretchSpacer()

        self.name = self.add_field(
            label=_('Name'),
            parent=top_sizer,
            ctrl=wx.TextCtrl(self),
        )

        self.level = self.add_field(
            label=_('Level'),
            parent=top_sizer,
            ctrl=wx.SpinCtrl(self, min=1, max=20, value='1'),
        )

        top_sizer.AddStretchSpacer()

        buttons = self.CreateSeparatedButtonSizer(wx.OK | wx.CANCEL)
        top_sizer.Add(buttons, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())

        self.SetSizerAndFit(top_sizer)

        self.name.SetFocus()

    def add_field(self, label, parent, ctrl):
        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, label)
        sizer.Add(ctrl, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        parent.Add(sizer, wx.SizerFlags().DoubleBorder(wx.LEFT | wx.RIGHT).Expand())
        return ctrl

class AddMonsterDialog(wx.Dialog):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, title=_('Add New Monster'), **kwargs)

        self.__monsters = load_monsters()

        top_sizer = wx.BoxSizer(wx.VERTICAL)

        top_sizer.AddStretchSpacer()

        self.name = self.add_field(
            label=_('Name'),
            parent=top_sizer,
            ctrl=wx.ComboBox(self, choices=list(self.__monsters.keys())),
        )

        self.cr = self.add_field(
            label=_('Challenge Rating'),
            parent=top_sizer,
            ctrl=wx.Choice(self, choices=list(map(str, CR))),
        )
        self.cr.SetSelection(4)

        top_sizer.AddStretchSpacer()

        buttons = self.CreateSeparatedButtonSizer(wx.OK | wx.CANCEL)
        top_sizer.Add(buttons, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())

        self.SetSizerAndFit(top_sizer)

        self.Bind(wx.EVT_COMBOBOX, self.combobox, self.name)

        self.name.SetFocus()

    def combobox(self, event):
        cr = self.__monsters[self.name.GetValue()]
        index = CR.index(Fraction(cr))
        self.cr.SetSelection(index)

    def add_field(self, label, parent, ctrl):
        sizer = wx.StaticBoxSizer(wx.VERTICAL, self, label)
        sizer.Add(ctrl, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        parent.Add(sizer, wx.SizerFlags().DoubleBorder(wx.LEFT | wx.RIGHT).Expand())
        return ctrl
