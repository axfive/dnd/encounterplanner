# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from .menubar import MenuBar
from .panel import Panel
import wx

class Frame(wx.Frame):
    '''The charsheet root frame'''

    def __init__(self, app, title):
        super().__init__(None, title=title)
        self.title = title
        self.name = None
        self.open_file = None

        self.app = app

        self.panel = Panel(app, self)

        self.menubar = MenuBar(app=app)
        self.SetMenuBar(self.menubar)

        self.CreateStatusBar()

        self.SetSize(self.DLG_UNIT(wx.Size(500,300)))

        self.refresh()

    def load(self, data):
        '''Load a data dict'''
        self.panel.load(data)

    def dump(self):
        '''Dump a data dict'''

        return self.panel.dump()

    def set_name(self, name=None):
        self.name = name
        self.refresh()

    def set_open_file(self, path):
        self.open_file = path
        self.refresh()

    def refresh(self):
        data = []

        if self.open_file:
            data.append(('File', str(self.open_file)))

        if self.name:
            data.append(('Plan', self.name))

        stringdata = ', '.join(': '.join(pair) for pair in data)

        if stringdata:
            self.SetTitle(f'{self.title} — {stringdata}')
        else:
            self.SetTitle(self.title)

        self.SetStatusText(stringdata)

    def dump_monsters(self):
        return self.panel.dump_monsters()
