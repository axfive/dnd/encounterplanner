#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from wx import dataview 
from gettext import gettext as _

from encounterplanner.encounter import Encounter, exp_to_cr
from encounterplanner import encounter

class EncounterModel(dataview.PyDataViewModel):
    COLUMN_TYPES = [
        'string',
        'string',
        'string',
        'string',
        'string',
    ]

    def __init__(self, encounters, players):
        super().__init__()
        self.encounters = encounters
        self.players = players
        self.UseWeakRefs(True)

    def GetColumnCount(self):
        return len(self.COLUMN_TYPES)

    def GetColumnType(self, col):
        return self.COLUMN_TYPES[col]

    def GetChildren(self, item, children):
        if not item:
            for encounter in self.encounters:
                children.append(self.ObjectToItem(encounter))
            return len(self.encounters)

        pyitem = self.ItemToObject(item)
        if isinstance(pyitem, Encounter):
            for monster in pyitem:
                children.append(self.ObjectToItem(monster))
            return len(children)
        return 0

    def IsContainer(self, item):
        if not item:
            return True
        pyitem = self.ItemToObject(item)

        return isinstance(pyitem, Encounter)

    def GetParent(self, item):
        if not item:
            return dataview.NullDataViewItem

        pyitem = self.ItemToObject(item)

        if isinstance(pyitem, Encounter):
            return dataview.NullDataViewItem

        for encounter in self.encounters:
            if encounter == pyitem.encounter:
                return self.ObjectToItem(encounter)

    def GetValue(self, item, col):
        pyitem = self.ItemToObject(item)

        if isinstance(pyitem, Encounter):
            multiplier = encounter.multiplier(len(self.players), len(pyitem))
            threshold = encounter.party_threshold(player.level for player in self.players)
            modified_experience = pyitem.experience * multiplier
            cr = exp_to_cr(modified_experience)
            experience = f'{pyitem.experience} (mod: {modified_experience})'
            mapper = [
                pyitem.name,
                f'{cr:.02f}',
                experience,
                str(multiplier),
                _(threshold.which(modified_experience)),
            ]
        else:
            mapper = [
                pyitem.name,
                str(pyitem.cr),
                str(pyitem.experience),
                '',
                '',
            ]

        return mapper[col]

    def SetValue(self, value, item, col):
        pyitem = self.ItemToObject(item)
        if isinstance(pyitem, Encounter) and col == 0:
            pyitem.name = value
            return True
        return False

    def AddEncounter(self, encounter):
        '''Adds the encounter, and returns the encounter for selection as an item'''
        self.encounters.append(encounter)
        encounter_item = self.ObjectToItem(encounter)
        self.ItemAdded(dataview.NullDataViewItem, encounter_item)
        for monster in encounter:
            monster_item = self.ObjectToItem(monster)
            self.ItemAdded(encounter_item, monster_item)
        return encounter_item

    def AddEncounterMonster(self, encounter_monster):
        '''Adds the encounter monster, and returns the monster for selection as an item'''
        encounter_monster.encounter.append(encounter_monster)
        encounter = self.ObjectToItem(encounter_monster.encounter)
        encounter_monster = self.ObjectToItem(encounter_monster)
        self.ItemAdded(encounter, encounter_monster)
        return encounter_monster

    def DeleteEncounter(self, encounter):
        '''Deletes the Encounter'''
        self.encounters.remove(encounter)
        encounter = self.ObjectToItem(encounter)
        self.ItemDeleted(dataview.NullDataViewItem, encounter)

    def DeleteEncounterMonster(self, monster):
        '''Deletes the Encounter'''
        encounter = monster.encounter
        encounter.remove(monster)
        self.ItemDeleted(
            self.ObjectToItem(encounter),
            self.ObjectToItem(monster),
        )

    def HasContainerColumns(self, item):
        # All levels need all columns
        return True
