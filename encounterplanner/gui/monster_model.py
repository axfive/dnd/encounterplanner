#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from wx import dataview 

class MonsterModel(dataview.PyDataViewModel):
    COLUMN_TYPES = [
        'string',
        'string',
        'string',
    ]

    def __init__(self, data):
        super().__init__()
        self.data = data
        self.UseWeakRefs(True)

    def GetColumnCount(self):
        return len(self.COLUMN_TYPES)

    def GetColumnType(self, col):
        return self.COLUMN_TYPES[col]

    def GetChildren(self, item, children):
        # Not a tree
        if item:
            return 0
        else:
            for player in self.data:
                children.append(self.ObjectToItem(player))
            return len(self.data)

    def IsContainer(self, item):
        # Only root is container
        return not item

    def GetParent(self, item):
        return dataview.NullDataViewItem

    def GetValue(self, item, col):
        pyitem = self.ItemToObject(item)

        mapper = [
            pyitem.name,
            str(pyitem.cr),
            str(pyitem.experience),
        ]

        return mapper[col]

    def SetValue(self, value, item, col):
        pyitem = self.ItemToObject(item)

        if col == 0:
            pyitem.name = value
        elif col == 1:
            pyitem.cr = value
        else:
            return False

        return True

    def AddMonster(self, monster):
        '''Add the monster, and return its item for selection'''
        self.data.append(monster)
        monster = self.ObjectToItem(monster)
        self.ItemAdded(dataview.NullDataViewItem, monster)
        return monster

    def DeleteMonster(self, monster):
        '''Remove the monster'''
        self.data.remove(monster)
        self.ItemDeleted(dataview.NullDataViewItem, self.ObjectToItem(monster))
