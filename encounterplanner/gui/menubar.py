#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

import wx

class MenuBar(wx.MenuBar):
    def __init__(self, app):
        super().__init__()

        self.app = app

        fileMenu = wx.Menu()
        open = fileMenu.Append(-1, "&Open\tCtrl-O", "Open a saved encounter plan.")
        save = fileMenu.Append(-1, "&Save\tCtrl-S", "Save this encounter plan.")
        save_as = fileMenu.Append(-1, "Save &As...\tCtrl-Shift-S", "Save this encounter plan as a new file.")
        fileMenu.AppendSeparator()
        exit = fileMenu.Append(wx.ID_EXIT)

        monsterMenu = wx.Menu()
        commit_monsters = monsterMenu.Append(-1, "&Commit monsters\tCtrl-Shift-C", "Commit all monsters in your list to your selectable monster list.")
        import_monsters = monsterMenu.Append(-1, "&Import monsters\tCtrl-I", "Import an external monster list for selection.")
        export_monsters = monsterMenu.Append(-1, "&Export monsters\tCtrl-E", "Export your committed monster list to a file.")

        helpMenu = wx.Menu()
        about = helpMenu.Append(wx.ID_ABOUT)

        self.Append(fileMenu, "&File")
        self.Append(monsterMenu, "&Monsters")
        self.Append(helpMenu, "&Help")


        self.Bind(wx.EVT_MENU, self.open, open)
        self.Bind(wx.EVT_MENU, self.save, save)
        self.Bind(wx.EVT_MENU, self.save_as, save_as)
        self.Bind(wx.EVT_MENU, self.exit,  exit)
        self.Bind(wx.EVT_MENU, self.commit_monsters, commit_monsters)
        self.Bind(wx.EVT_MENU, self.import_monsters, import_monsters)
        self.Bind(wx.EVT_MENU, self.export_monsters, export_monsters)
        self.Bind(wx.EVT_MENU, self.about, about)

    def exit(self, event):
        self.app.Close()

    def open(self, event):
        self.app.open()

    def save(self, event):
        self.app.save()

    def save_as(self, event):
        self.app.save_as()

    def commit_monsters(self, event):
        self.app.commit_monsters()

    def import_monsters(self, event):
        self.app.import_monsters()

    def export_monsters(self, event):
        self.app.export_monsters()

    def about(self, event):
        self.app.about()
