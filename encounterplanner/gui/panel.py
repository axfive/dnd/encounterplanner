#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from .dialogs import AddPlayerDialog, AddMonsterDialog
from .player_model import PlayerModel
from .encounter_model import EncounterModel
from .monster_model import MonsterModel
from fractions import Fraction
from encounterplanner.monster import Monster, CR
from encounterplanner.encounter import Encounter, EncounterMonster
from encounterplanner.player import Player
from gettext import gettext as _
from wx import dataview 
import wx

def split(it, predicate):
    '''Split the given iterable to two output lists (True, False) based on a given predicate'''
    true = []
    false = []

    for item in it:
        if predicate(item):
            true.append(item)
        else:
            false.append(item)

    return true, false

class Panel(wx.Panel):
    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app

        top_sizer = wx.BoxSizer(wx.HORIZONTAL)

        players_sizer = wx.StaticBoxSizer(wx.VERTICAL, self, _('Players'))
        encounters_sizer = wx.StaticBoxSizer(wx.VERTICAL, self, _('Encounters'))
        monsters_sizer = wx.StaticBoxSizer(wx.VERTICAL, self, _('Monsters'))
        add_encounter_monster_button_sizer = wx.BoxSizer(wx.VERTICAL)

        add_encounter_monster_button = wx.Button(self, label=_('<-'))
        add_encounter_monster_button_sizer.AddStretchSpacer()
        add_encounter_monster_button_sizer.Add(add_encounter_monster_button)
        add_encounter_monster_button_sizer.AddStretchSpacer()

        add_player_button = wx.Button(self, label=_('Add Player'))
        delete_player_button = wx.Button(self, label=_('Delete Player'))
        players_sizer.Add(add_player_button, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        players_sizer.Add(delete_player_button, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        self.players = []
        self.players_view = dataview.DataViewCtrl(self, size=self.DLG_UNIT(wx.Size(20, 300)))
        players_sizer.Add(self.players_view, wx.SizerFlags().DoubleBorder(wx.ALL).Proportion(1).Expand())
        self.players_model = PlayerModel(self.players)
        self.players_view.AssociateModel(self.players_model)
        self.players_view.AppendTextColumn(
            label=_('Name'),
            model_column=0,
            width=self.DLG_UNIT(wx.Size(75, 0)).GetWidth(),
            mode=dataview.DATAVIEW_CELL_EDITABLE,
        )
        self.players_view.AppendTextColumn(
            label=_('Level'),
            model_column=1,
            mode=dataview.DATAVIEW_CELL_EDITABLE,
        )

        add_monster_button = wx.Button(self, label=_('Add Monster'))
        delete_monster_button = wx.Button(self, label=_('Delete Monster'))
        monsters_sizer.Add(add_monster_button, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        monsters_sizer.Add(delete_monster_button, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        self.monsters = []

        self.monsters_view = dataview.DataViewCtrl(self, size=self.DLG_UNIT(wx.Size(20, 300)))
        monsters_sizer.Add(self.monsters_view, wx.SizerFlags().DoubleBorder(wx.ALL).Proportion(1).Expand())
        self.monsters_model = MonsterModel(self.monsters)
        self.monsters_view.AssociateModel(self.monsters_model)
        self.monsters_view.AppendTextColumn(
            label=_('Name'),
            model_column=0,
            #width=self.DLG_UNIT(wx.Size(75, 0)).GetWidth(),
            mode=dataview.DATAVIEW_CELL_EDITABLE,
        )
        self.monsters_view.AppendTextColumn(
            label=_('CR'),
            model_column=1,
            mode=dataview.DATAVIEW_CELL_EDITABLE,
        )
        self.monsters_view.AppendTextColumn(
            label=_('Exp'),
            model_column=2,
        )

        add_encounter_button = wx.Button(self, label=_('Add Encounter'))
        delete_encounter_button = wx.Button(self, label=_('Delete Encounter/Monster'))
        encounters_sizer.Add(add_encounter_button, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        encounters_sizer.Add(delete_encounter_button, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        self.encounters = []
        self.encounters_model = EncounterModel(
            encounters=self.encounters,
            players=self.players,
        )
        self.encounters_view = dataview.DataViewCtrl(self, size=self.DLG_UNIT(wx.Size(20, 300)))
        encounters_sizer.Add(self.encounters_view, wx.SizerFlags().DoubleBorder(wx.ALL).Proportion(1).Expand())

        self.encounters_view.AssociateModel(self.encounters_model)
        self.encounters_view.AppendTextColumn(
            label=_('Name'),
            model_column=0,
            mode=dataview.DATAVIEW_CELL_EDITABLE,
        )
        self.encounters_view.AppendTextColumn(
            label=_('CR'),
            model_column=1,
        )
        self.encounters_view.AppendTextColumn(
            label=_('Exp'),
            model_column=2,
        )
        self.encounters_view.AppendTextColumn(
            label=_('Multiplier'),
            model_column=3,
        )
        self.encounters_view.AppendTextColumn(
            label=_('Difficulty'),
            model_column=4,
        )

        self.Bind(wx.EVT_BUTTON, self.add_player_button_pressed, add_player_button)
        self.Bind(wx.EVT_BUTTON, self.add_encounter_button_pressed, add_encounter_button)
        self.Bind(wx.EVT_BUTTON, self.add_encounter_monster_button_pressed, add_encounter_monster_button)
        self.Bind(wx.EVT_BUTTON, self.add_monster_button_pressed, add_monster_button)

        self.Bind(wx.EVT_BUTTON, self.delete_player_button_pressed, delete_player_button)
        self.Bind(wx.EVT_BUTTON, self.delete_encounter_button_pressed, delete_encounter_button)
        self.Bind(wx.EVT_BUTTON, self.delete_monster_button_pressed, delete_monster_button)

        self.Bind(dataview.EVT_DATAVIEW_ITEM_VALUE_CHANGED, self.player_changed, self.players_view)
        self.Bind(dataview.EVT_DATAVIEW_ITEM_VALUE_CHANGED, self.monster_changed, self.monsters_view)

        top_sizer.Add(players_sizer, wx.SizerFlags().TripleBorder(wx.ALL).Proportion(1).Expand())
        top_sizer.Add(encounters_sizer, wx.SizerFlags().TripleBorder(wx.ALL).Proportion(1).Expand())
        top_sizer.Add(add_encounter_monster_button_sizer, wx.SizerFlags().TripleBorder(wx.ALL).Expand())
        top_sizer.Add(monsters_sizer, wx.SizerFlags().TripleBorder(wx.ALL).Proportion(1).Expand())

        self.SetSizer(top_sizer)

    def player_changed(self, event):
        for encounter in self.encounters:
            self.encounters_model.ItemChanged(self.encounters_model.ObjectToItem(encounter))

    def monster_changed(self, event):
        changed_monster = self.monsters_model.ItemToObject(event.GetItem())
        # Have to seek out and find monsters
        for encounter in self.encounters:
            for monster in encounter:
                if monster.monster is changed_monster:
                    self.encounters_model.ItemChanged(self.encounters_model.ObjectToItem(monster))

    def add_player_button_pressed(self, event):
        dialog = AddPlayerDialog(self.app.frame)
        try:
            if dialog.ShowModal() == wx.ID_OK:
                self.add_player(
                    name=dialog.name.GetValue(),
                    level=int(dialog.level.GetValue()),
                )
        finally:
            dialog.Destroy()

    def delete_player_button_pressed(self, event):
        if self.players_view.HasSelection():
            player_item = self.players_view.GetSelection()
            player = self.players_model.ItemToObject(player_item)
            self.players_model.DeletePlayer(player)

            for encounter in self.encounters:
                self.encounters_model.ItemChanged(self.encounters_model.ObjectToItem(encounter))

    def delete_monster_button_pressed(self, event):
        if self.monsters_view.HasSelection():
            monster_item = self.monsters_view.GetSelection()
            delete_monster = self.monsters_model.ItemToObject(monster_item)
            self.monsters_model.DeleteMonster(delete_monster)

            for encounter in self.encounters:
                delete, keep = split(encounter, lambda monster: monster.monster is delete_monster)
                encounter[:] = keep
                for monster in delete:
                    self.encounters_model.ItemDeleted(
                        self.encounters_model.ObjectToItem(encounter),
                        self.encounters_model.ObjectToItem(monster),
                    )

    def delete_encounter_button_pressed(self, event):
        if self.encounters_view.HasSelection():
            delete_item = self.encounters_model.ItemToObject(self.encounters_view.GetSelection())
            if isinstance(delete_item, Encounter):
                self.encounters_model.DeleteEncounter(delete_item)
            elif isinstance(delete_item, EncounterMonster):
                self.encounters_model.DeleteEncounterMonster(delete_item)

    def add_encounter_button_pressed(self, event):
        encounter = Encounter()
        encounter = self.encounters_model.AddEncounter(encounter)
        self.encounters_view.Select(encounter)

    def add_encounter_monster_button_pressed(self, event):
        if self.encounters_view.HasSelection() and self.monsters_view.HasSelection():
            encounter = self.encounters_model.ItemToObject(self.encounters_view.GetSelection())

            # If not an encounter is highlighted, but instead a monster
            if isinstance(encounter, EncounterMonster):
                encounter = encounter.encounter

            monster = self.monsters_model.ItemToObject(self.monsters_view.GetSelection())

            encounter_monster = EncounterMonster(encounter=encounter, monster=monster)
            monster = self.encounters_model.AddEncounterMonster(encounter_monster)
            self.encounters_view.ExpandAncestors(monster)
            self.encounters_view.Select(monster)

        else:
            dialog = wx.GenericMessageDialog(self, _('You must have both an encounter and a monster selected'))
            try:
                dialog.ShowModal()
            finally:
                dialog.Destroy()

    def add_monster_button_pressed(self, event):
        dialog = AddMonsterDialog(self.app.frame)
        try:
            if dialog.ShowModal() == wx.ID_OK:
                cr = CR[dialog.cr.GetSelection()]
                self.add_monster(
                    name=dialog.name.GetValue(),
                    cr=cr,
                )
        finally:
            dialog.Destroy()

    def add_player(self, name, level):
        player = Player(
            name=name,
            level=level,
        )
        player = self.players_model.AddPlayer(player)
        self.players_view.Select(player)

        for encounter in self.encounters:
            self.encounters_model.ItemChanged(self.encounters_model.ObjectToItem(encounter))

    def add_monster(self, name, cr):
        monster = Monster(
            name=name,
            cr=cr,
        )
        monster = self.monsters_model.AddMonster(monster)
        self.monsters_view.Select(monster)

    def load(self, data):
        '''Set this panel from the loaded  dict'''

        self.players.clear()
        self.monsters.clear()
        self.encounters.clear()

        self.players_model.Cleared()
        self.monsters_model.Cleared()
        self.encounters_model.Cleared()

        for player in data['players']:
            self.players_model.AddPlayer(Player(
                name=player['name'],
                level=player['level'],
            ))

        for monster in data['monsters']:
            self.monsters_model.AddMonster(Monster(
                name=monster['name'],
                cr=monster['cr'],
            ))

        for encounter in data['encounters']:
            encounter_item = self.encounters_model.AddEncounter(Encounter(
                name=encounter['name'],
                monsters=[EncounterMonster(monster=self.monsters[index]) for index in encounter['monsters']],
            ))
            if encounter['expanded']:
                self.encounters_view.Expand(encounter_item)
            else:
                self.encounters_view.Collapse(encounter_item)


    def dump(self):
        '''Get a dict representation of the data in this panel'''
        players = [
            {
                'name': player.name,
                'level': player.level,
            } for player in self.players
        ]
        monsters = [
            {
                'name': monster.name,
                'cr': str(monster.cr),
            } for monster in self.monsters
        ]
        encounters = [
            {
                'name': encounter.name,
                'expanded': self.encounters_view.IsExpanded(self.encounters_model.ObjectToItem(encounter)),
                'monsters': [self.monsters.index(monster.monster) for monster in encounter],
            } for encounter in self.encounters
        ]

        return {
            'players': players,
            'monsters': monsters,
            'encounters': encounters,
        }

    def dump_monsters(self):
        return {monster.name: monster.cr for monster in self.monsters}
