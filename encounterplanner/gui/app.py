#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from .._meta import version, about
from .frame import Frame
from encounterplanner.monster import load_monsters, update_monsters
from io import TextIOWrapper
from pathlib import Path
from zipfile import ZipFile, ZIP_DEFLATED
import json
import gzip
import wx
from encounterplanner.file import safe_write

class App(wx.App):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.frame = Frame(
            app=self,
            title=f'encounterplanner version {version}',
        )
        self.name = 'Encounter'
        self.previous_path = Path('.').resolve()
        self._open_file = None
        self.frame.Show()

    def open(self):
        '''Load the whole encounter plan'''

        path = wx.FileSelector(
            'Open an encounter plan',
            default_path=str(self.previous_path),
            wildcard='encounterplan Files (*.encounterplan)|*.encounterplan|All Files (*)|*',
            flags=wx.FD_OPEN|wx.FD_FILE_MUST_EXIST,
        )
        if not path:
            # Cancel
            return
        path = Path(path).resolve()

        self.previous_path = path.parent

        with gzip.open(path, 'rt') as file:
            self.frame.load(json.load(file))

        self.open_file = path

    def save(self):
        '''Save the encounter plan into the current open file if there is any, otherwise just do save_as'''
        if self.open_file:
            self.save_path(self.open_file)
        else:
            self.save_as()

    def save_as(self):
        '''Save into a file that is selected by the user'''

        path = wx.FileSelector(
            'Save this encounter plan',
            default_filename=f'{self.name}.encounterplan',
            default_path=str(self.previous_path),
            wildcard='encounterplan Files (*.encounterplan)|*.encounterplan|All Files (*)|*',
            flags=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT,
        )
        if not path:
            # Cancel
            return

        path = Path(path).resolve()

        self.previous_path = path.parent

        self.save_path(path)

        self.open_file = path

    def save_path(self, path):
        '''Save the encounter plan into the given path'''

        data = self.frame.dump()

        with safe_write(path, 'wb') as base:
            with gzip.open(base, 'wt') as file:
                json.dump(data, file, indent=2)

    def name_changed(self, name=None):
        self.name = name
        self.frame.set_name(name=name)

    def about(self):
        wx.MessageBox(about,
                      "About encounterplanner",
                      wx.OK|wx.ICON_INFORMATION)

    @property
    def open_file(self):
        return self._open_file

    @open_file.setter
    def open_file(self, value):
        self._open_file = value
        self.frame.set_open_file(path=value)

    def commit_monsters(self):
        update_monsters(self.frame.dump_monsters())

    def import_monsters(self):
        path = wx.FileSelector(
            'Import a monsterexport',
            default_path=str(self.previous_path),
            wildcard='monsterexport Files (*.monsterexport)|*.monsterexport|All Files (*)|*',
            flags=wx.FD_OPEN|wx.FD_FILE_MUST_EXIST,
        )
        if not path:
            # Cancel
            return

        path = Path(path).resolve()

        self.previous_path = path.parent

        with gzip.open(path, 'rt') as file:
            update_monsters(json.load(file))

    def export_monsters(self):
        path = wx.FileSelector(
            'Make a monsterexport',
            default_filename=f'{self.name}.monsterexport',
            default_path=str(self.previous_path),
            wildcard='monsterexport Files (*.monsterexport)|*.monsterexport|All Files (*)|*',
            flags=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT,
        )
        if not path:
            # Cancel
            return

        path = Path(path).resolve()

        self.previous_path = path.parent

        with safe_write(path, 'wb') as base:
            with gzip.open(base, 'wt') as file:
                json.dump({key: str(value) for key, value in load_monsters().items()}, file)

    def Close(self, force=False):
        self.frame.Close(force=force)
