#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from wx import dataview 

class PlayerModel(dataview.PyDataViewModel):
    COLUMN_TYPES = [
        'string',
        'string',
    ]

    def __init__(self, data):
        super().__init__()
        self.data = data
        self.UseWeakRefs(True)

    def GetColumnCount(self):
        return len(self.COLUMN_TYPES)

    def GetColumnType(self, col):
        return self.COLUMN_TYPES[col]

    def GetChildren(self, item, children):
        # Not a tree
        if item:
            return 0
        else:
            for player in self.data:
                children.append(self.ObjectToItem(player))
            return len(self.data)

    def IsContainer(self, item):
        # Only root is container
        return not item

    def GetParent(self, item):
        return dataview.NullDataViewItem

    def GetValue(self, item, col):
        pyitem = self.ItemToObject(item)

        mapper = [
            pyitem.name,
            str(pyitem.level),
        ]

        return mapper[col]

    def SetValue(self, value, item, col):
        pyitem = self.ItemToObject(item)

        if col == 0:
            pyitem.name = value
        elif col == 1:
            pyitem.level = int(value)
        else:
            return False

        return True

    def AddPlayer(self, player):
        '''Add the player, and return its item for selection'''
        self.data.append(player)
        player = self.ObjectToItem(player)
        self.ItemAdded(dataview.NullDataViewItem, player)
        return player

    def DeletePlayer(self, player):
        '''Remove the player'''
        self.data.remove(player)
        self.ItemDeleted(dataview.NullDataViewItem, self.ObjectToItem(player))
