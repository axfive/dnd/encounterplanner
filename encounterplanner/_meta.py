# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger and Brandon Phillips
# This code is released under the license described in the LICENSE file

from packaging.version import Version

version = Version('0.0.4')

data = dict(
    name='encounterplanner',
    version=str(version),
    author='Taylor C. Richberger and Brandon Phillips',
    description='A simple program for planning D&D encounters',
    license='MIT',
    keywords='dnd dungeons dragons rpg game utility',
    url='https://gitlab.com/Taywee/encounterplanner',
    entry_points=dict(
        gui_scripts=[
            'encounterplanner = encounterplanner.__main__:main',
        ],
    ),
    packages=[
        'encounterplanner',
        'encounterplanner.gui',
    ],
    package_data = {
        'encounterplanner': ['srd_monsters.json'],
    },
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'License :: OSI Approved :: MIT License',
        'Topic :: Utilities',
        'Topic :: Games/Entertainment',
        'Topic :: Games/Entertainment :: Role-Playing',
    ],
)

_about = '''
This is encounterplanner, a simple program for planning potential D&D
encounters.

General use is to add players on the left, add monsters on the right, and then
build encounters in the middle by initializing encounters and putting monsters
into it.

You can add custom monsters you've added on the right to your monster
suggestion list with Commit in the Monsters menu.  You can then export your
monster set for others to use, and import others' monster lists.

This is free software.  It is written by and copyright 2019 Taylor C.
Richberger.
'''

import re

_linebreakremover = re.compile(r'(\S)\n(\S)')

about = _linebreakremover.sub(r'\1 \2', _about.strip())
