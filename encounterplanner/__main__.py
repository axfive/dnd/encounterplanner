#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger and Brandon Phillips
# This code is released under the license described in the LICENSE file

from . import _meta
from .gui.app import App
import argparse
import locale
import sys
from pathlib import Path

def main():
    locale.setlocale(locale.LC_ALL, '')
    parser = argparse.ArgumentParser(description='Plan an encounter')
    parser.add_argument('-V', '--version', action='version', version=str(_meta.version))
    args = parser.parse_args()
    # A dumb hack to get around mac OSX stupid menu name generation
    sys.argv[0] = str(Path(sys.argv[0]).parent.parent / 'Encounter Planner')

    app = App()
    app.MainLoop()

if __name__ == '__main__':
    main()
