#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

class Player:
    def __init__(self, name, level):
        self.name = name
        self.level = level

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        value = int(value)
        if value in range(1, 21):
            self._level = value
        else:
            raise RuntimeError(f'level {value} not a legal level')
